Tabta - your personal Tabata timer
================

Loads all Tabata timers stored in database and displays them. Allows for modification of existing timers as well as adding new ones. 

# Project architecture

This project uses MVVM architecture supported by [Clean architecture](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html) approach. Some aspects may differ from perfect adaptation but they will be improved in later stages of the project.

Following reasons convinced me to use this architecture:

* When models and data will be implemented completely app will be really easy to test.
* Application is UI independent because of use cases which can be easily reused even on different platform.
* App can be independent of database. Changing database requires only modifications in one or two layers (Service layer and mapping in Repository layer) without the need to largely modify app code.
* I have experience with similar architectures in other projects which tended to have some layers modified in order to support specific needs of the project.

# Project status

Amount of hours spent on code: ~11

Application is in very early stage but should be sufficient to present some amout of my skills in regard to Swift, Rx libraries and app architecture knowledge/experience. This is a personal project I was putting off so it will be improved further.

Following features are implemented:

* Basic UI allowing for app usage including splash screen and app icon
* Displaying list of stored timers extracted from database
* Creating new timers and saving them into database
* Displayng details of each timer without the possibility to edit them

Following features are planned for next phase:

* Error handling
* Timers eddition and removal
* Timers starting in foreground as well as background task
* Improved UI
* iWatch app / face
* Workout statistics (route display, health stats, cool UI)
* Simple backend integration
* Share via social media


# External dependencies

The project uses following external dependencies:

* [RxSwift & RxCocoa(4.0)](https://github.com/ReactiveX/RxSwift)
* [EmptyDataSet-Swift (4.0.1)](https://github.com/Xiaoye220/EmptyDataSet-Swift)
* [Swinject (2.4)](https://github.com/Swinject/Swinject)
* [SwinjectStoryboard (2.0.2)](https://github.com/Swinject/SwinjectStoryboard)
* [Eureka](https://github.com/xmartlabs/Eureka)
* [RxEureka](https://github.com/antoninbiret/RxEureka)

All dependencies are integrated using [CocoaPods](https://cocoapods.org) which usage is described bellow.

# Dependency Management

The project has some dependencies to third-party libraries. They are managed via [CocoaPods](https://cocoapods.org).

In order to manage external tools versioning for all developers it is recommended to use [Bundler](https://bundler.io) and following command in terminal:

`bundle install --path vendor/bundle`

which will install required external tools in correct versions in `vendor/bundle` directory.

In order to run one of external tools for example CocoaPods the following command should be used:

`bundle exec pod install`




