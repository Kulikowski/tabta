//
//  AppSegues.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import Foundation

struct AppSegues {
    struct WorkoutsList {
        static let goToAddWorkout = "goToAddWorkoutSegue"
        static let goToEditWorkout = "goToEditWorkoutSegue"
        static let goToStartWorkout = "goToStartWorkoutSegue"
    }
}
