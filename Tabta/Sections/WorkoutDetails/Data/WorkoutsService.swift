//
//  WorkoutsService.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import UIKit
import CoreData

class WorkoutsService {

    func getWorkouts(completion: @escaping ((Result<[WorkoutManagedObject], Error>) -> Void)) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            //TODO: return a service based error here
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<WorkoutManagedObject>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "WorkoutManagedObject", in: managedContext)
        fetchRequest.entity = entityDescription

        do {
            let results = try managedContext.fetch(fetchRequest)
            completion(Result.success(results))
        } catch {
            completion(Result.error(error))
        }
    }

    func saveWorkout(withParameters parameters: WorkoutParameters, completion: @escaping (Result<Bool, Error>) -> Void) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext

        let workout = NSEntityDescription.insertNewObject(forEntityName: "WorkoutManagedObject",
                                                         into: managedContext)
        workout.setValue(parameters.exerciseInterval, forKey: "exercise_interval")
        workout.setValue(parameters.initialCountdown, forKey: "initial_countdown")
        workout.setValue(parameters.name, forKey: "workout_name")
        workout.setValue(parameters.numberOfSets, forKey: "number_of_sets")
        workout.setValue(parameters.recoveryInterval, forKey: "recovery_interval")
        workout.setValue(parameters.restInterval, forKey: "rest_interval")
        workout.setValue(parameters.warmupInterval, forKey: "warmup_interval")
        workout.setValue(parameters.cooldownInterval, forKey: "cooldown_interval")

        do {
            try managedContext.save()
        } catch {
            completion(Result.error(error))
        }

        completion(.success(true))
    }
}
