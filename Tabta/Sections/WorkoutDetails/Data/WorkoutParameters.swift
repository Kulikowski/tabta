//
//  WorkoutParameters.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import Foundation

struct WorkoutParameters {
    let id: String?
    let name: String
    let initialCountdown: Int
    let warmupInterval: Int
    let exerciseInterval: Int
    let restInterval: Int
    let numberOfSets: Int
    let recoveryInterval: Int
    let cooldownInterval: Int
}
