//
//  WorkoutDetailsTableViewController.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Eureka
import RxEureka

class WorkoutDetailsViewController: FormViewController {

    let disposeBag = DisposeBag()

    var viewModel: WorkoutDetailsViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViewModelBindings()

        setupUI()
    }

    // MARK: ViewModel bindings

    private func setupViewModelBindings() {
        viewModel.isLoading.asObservable().subscribe(onNext: { isLoading in
            if !isLoading {
                // hide loading indicator
            }
        }).disposed(by: disposeBag)

        if viewModel.showSaveButton.value {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.save, target: self, action: #selector(saveWorkout))
        } else {
            self.navigationItem.rightBarButtonItem = nil
        }
        viewModel.showSaveButton.asObservable().subscribe(onNext: { [weak self] value in
            guard let `self` = self else { return }
            if value {
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.save, target: self, action: #selector(self.saveWorkout))
            } else {
                self.navigationItem.rightBarButtonItem = nil
            }
        }).disposed(by: disposeBag)

        viewModel.isSaveEnabled.asObservable().subscribe(onNext: { [weak self] value in
            guard let `self` = self else { return }
            self.navigationItem.rightBarButtonItem?.isEnabled = value
        }).disposed(by: disposeBag)

        viewModel.workoutSaveResult.asObservable().subscribe(onNext: { [weak self] result in
            guard let `self` = self, let result = result else { return }
            if result {
                self.navigationController?.popViewController(animated: true)
            } else {
                //TODO: add error notification
            }
        }).disposed(by: disposeBag)

        viewModel.screenTitleString.asObservable().subscribe(onNext: { [weak self] title in
            guard let `self` = self else { return }
            self.title = title
        }).disposed(by: disposeBag)
    }

    // MARK: UI setup

    private func setupUI() {
        setupForm()
        setupNavigationBar()
    }

    private func setupForm() {
        for section: WorkoutDetailsSection in self.viewModel.sections {
            form +++ Section(section.title)

            for sectionRow: WorkoutDetailsSectionRow in section.rows {
                let section = form.last!
                switch sectionRow.type {
                case .workoutName(let name, let placeholder, let variable):
                    let row = TextRow() { row in
                        row.title = name
                        row.placeholder = placeholder
                        row.value = variable.value
                        row.disabled = Condition(booleanLiteral: variable.value != nil)
                    }
                    row.rx.value.asObservable()
                        .bind(to: variable)
                        .disposed(by: self.disposeBag)
                    section <<< row
                case .measureModel(let model):
                    switch model.type {
                    case .interval(let minutesRange, let secondsRange, let variable):
                        let minutesPluralFormat = NSLocalizedString("workout_details__measures__minutes_format", comment: "")
                        let secondsPluralFormat = NSLocalizedString("workout_details__measures__seconds_format", comment: "")
                        let row = DoublePickerInputRow<String, String>() {
                            $0.firstOptions = { return minutesRange.rangeStrings(withSuffixPluralFormat: minutesPluralFormat) }
                            $0.secondOptions = { _ in return secondsRange.rangeStrings(withSuffixPluralFormat: secondsPluralFormat) }
                            $0.title = model.title
                            $0.value = variable.value
                            $0.disabled = Condition(booleanLiteral: variable.value != nil)
                        }
                        row.rx.value.asObservable()
                            .bind(to: variable)
                            .disposed(by: self.disposeBag)
                        section <<< row
                    case .sets(let range, let variable):
                        let setsPluralFormat = NSLocalizedString("workout_details__measures__sets_count_format", comment: "")
                        let row = PickerInputRow<String>() {
                            $0.options.append(contentsOf: range.rangeStrings(withSuffixPluralFormat: setsPluralFormat))
                            $0.title = model.title
                            $0.value = variable.value
                            $0.disabled = Condition(booleanLiteral: variable.value != nil)
                        }
                        row.rx.value.asObservable()
                            .bind(to: variable)
                            .disposed(by: self.disposeBag)
                        section <<< row
                    }
                }
            }
        }
    }

    private func setupNavigationBar() {
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.title = self.viewModel.screenTitleString.value

        let backImage = UIImage(named: "BackArrow")?.withRenderingMode(.alwaysTemplate)
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage,
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(goBack))
    }

    // MARK: UI actions:

    @objc func saveWorkout() {
        viewModel.saveWorkout()
    }

    @objc func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
}
