//
//  SaveWorkoutUseCase.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import Foundation

class SaveWorkoutsUseCase {

    let workoutsRepository: WorkoutsRepository

    init(workoutsRepository: WorkoutsRepository) {
        self.workoutsRepository = workoutsRepository
    }

    func save(workout: Workout, completion: @escaping ((Result<Bool, Error>) -> Void)) {
        self.workoutsRepository.save(workout: workout, completion: completion)
    }
}
