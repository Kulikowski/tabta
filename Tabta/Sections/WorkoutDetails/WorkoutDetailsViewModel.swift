//
//  WorkoutDetailsViewModel.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import UIKit
import RxSwift
import Eureka

class WorkoutDetailsViewModel: NSObject {

    let disposeBag = DisposeBag()

    let saveWorkoutsUseCase: SaveWorkoutsUseCase

    var isLoading = Variable(false)
    var isSaveEnabled = Variable<Bool>(false)
    var showSaveButton = Variable(true)
    var workoutSaveResult = Variable<Bool?>(nil)

    var screenTitleString: Variable<String> = Variable(NSLocalizedString("workout_details__screen_title_string__new_workout",
                                                                         comment: ""))

    var sections: [WorkoutDetailsSection] {
        let titleString = NSLocalizedString("workout_details__workout_name__title", comment: "")
        let placeholderString = NSLocalizedString("workout_details__workout_name__placeholder", comment: "")
        return [
            WorkoutDetailsSection(title: NSLocalizedString("workout_details__section_title__name", comment: ""),
                                  rows: [WorkoutDetailsSectionRow(type: .workoutName(name: titleString,
                                                                                     placeholder: placeholderString,
                                                                                     variable: workoutName))]),
            WorkoutDetailsSection(title: NSLocalizedString("workout_details__section_title__measures", comment: ""),
                                  rows: self.measures)
        ]
    }

    private var measures: [WorkoutDetailsSectionRow] {
        let models = [
            WorkoutMeasureModel(title: NSLocalizedString("workout_details__measures__initial_countdown", comment: ""),
                                type: .interval(minutesRange: WorkoutRange(min: 1, max: 90),
                                                secondsRange: WorkoutRange(min: 0, max: 59),
                                                variable: initialCountdown)),
            WorkoutMeasureModel(title: NSLocalizedString("workout_details__measures__warmup_interval", comment: ""),
                                type: .interval(minutesRange: WorkoutRange(min: 1, max: 90),
                                                secondsRange: WorkoutRange(min: 0, max: 59),
                                                variable: warmupInterval)),
            WorkoutMeasureModel(title: NSLocalizedString("workout_details__measures__exercise_interval", comment: ""),
                                type: .interval(minutesRange: WorkoutRange(min: 1, max: 90),
                                                secondsRange: WorkoutRange(min: 0, max: 59),
                                                variable: exerciseInterval)),
            WorkoutMeasureModel(title: NSLocalizedString("workout_details__measures__rest_interval", comment: ""),
                                type: .interval(minutesRange: WorkoutRange(min: 1, max: 90),
                                                secondsRange: WorkoutRange(min: 0, max: 59),
                                                variable: restInterval)),
            WorkoutMeasureModel(title: NSLocalizedString("workout_details__measures__number_of_sets", comment: ""),
                                type: .sets(range: WorkoutRange(min: 1, max: 99),
                                            variable: numberOfSets)),
            WorkoutMeasureModel(title: NSLocalizedString("workout_details__measures__recovery_interval", comment: ""),
                                type: .interval(minutesRange: WorkoutRange(min: 1, max: 90),
                                                secondsRange: WorkoutRange(min: 0, max: 59),
                                                variable: recoveryInterval)),
            WorkoutMeasureModel(title: NSLocalizedString("workout_details__measures__cooldown_interval", comment: ""),
                                type: .interval(minutesRange: WorkoutRange(min: 1, max: 90),
                                                secondsRange: WorkoutRange(min: 0, max: 59),
                                                variable: cooldownInterval))
        ]

        return models.map({ return WorkoutDetailsSectionRow(type: .measureModel(model: $0)) })
    }

    let workoutID: Variable<String?> = Variable(nil)
    let workoutName: Variable<String?> = Variable(nil)
    let initialCountdown: Variable<Tuple<String, String>?> = Variable(nil)
    let warmupInterval: Variable<Tuple<String, String>?> = Variable(nil)
    let exerciseInterval: Variable<Tuple<String, String>?> = Variable(nil)
    let restInterval: Variable<Tuple<String, String>?> = Variable(nil)
    let numberOfSets: Variable<String?> = Variable(nil)
    let recoveryInterval: Variable<Tuple<String, String>?> = Variable(nil)
    let cooldownInterval: Variable<Tuple<String, String>?> = Variable(nil)

    init(saveWorkoutsUseCase: SaveWorkoutsUseCase) {
        self.saveWorkoutsUseCase = saveWorkoutsUseCase

        super.init()

        setupBindings()
    }

    func update(with workout: Workout) {
        workoutID.value = workout.id
        workoutName.value = workout.name
        initialCountdown.value = workout.initialCountdown.asTimeStringsTuple()
        warmupInterval.value = workout.warmupInterval.asTimeStringsTuple()
        exerciseInterval.value = workout.exerciseInterval.asTimeStringsTuple()
        restInterval.value = workout.restInterval.asTimeStringsTuple()
        numberOfSets.value = workout.numberOfSets.asSetsString()
        recoveryInterval.value = workout.recoveryInterval.asTimeStringsTuple()
        cooldownInterval.value = workout.cooldownInterval.asTimeStringsTuple()
        showSaveButton.value = false
        screenTitleString.value = String(format: NSLocalizedString("workout_details__screen_title_string__existing_workout_format", comment: ""), workout.name)
    }

    private func setupBindings() {
        Observable.combineLatest(workoutName.asObservable().distinctUntilChanged(),
                                 initialCountdown.asObservable().distinctUntilChanged(),
                                 warmupInterval.asObservable().distinctUntilChanged(),
                                 exerciseInterval.asObservable().distinctUntilChanged(),
                                 restInterval.asObservable().distinctUntilChanged(),
                                 recoveryInterval.asObservable().distinctUntilChanged(),
                                 cooldownInterval.asObservable().distinctUntilChanged(),
                                 numberOfSets.asObservable().distinctUntilChanged())
            .map {(name, countdown, warmup, exercise, rest, recovery, cooldown, sets) in
                return name != nil
                    && countdown != nil
                    && warmup != nil
                    && exercise != nil
                    && rest != nil
                    && recovery != nil
                    && cooldown != nil
                    && sets != nil
            }
            .bind(to: isSaveEnabled)
            .disposed(by: disposeBag)
    }

    func saveWorkout() {
        guard let name = workoutName.value,
            let exerciseInterval = TupleMapper.mapToInt(from: exerciseInterval.value),
            let initialCountdown = TupleMapper.mapToInt(from: initialCountdown.value),
            let numberOfSetsValue = numberOfSets.value,
            let numberOfSets = numberOfSetsValue.extractFirstNumber(),
            let recoveryInterval = TupleMapper.mapToInt(from: recoveryInterval.value),
            let restInterval = TupleMapper.mapToInt(from: restInterval.value),
            let warmupInterval = TupleMapper.mapToInt(from: warmupInterval.value),
            let cooldownInterval = TupleMapper.mapToInt(from: cooldownInterval.value) else { return }
        let workout = Workout(id: workoutID.value,
                              name: name,
                              exerciseInterval: exerciseInterval,
                              initialCountdown: initialCountdown,
                              numberOfSets: numberOfSets,
                              recoveryInterval: recoveryInterval,
                              restInterval: restInterval,
                              warmupInterval: warmupInterval,
                              cooldownInterval: cooldownInterval)
        saveWorkoutsUseCase.save(workout: workout) { [weak self] result in
            guard let `self` = self else { return }
            switch result {
            case .success(_):
                self.workoutSaveResult.value = true
            case .error(_):
                //TODO: show error
                self.workoutSaveResult.value = false
            }
        }
    }
}
