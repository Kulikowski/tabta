//
//  WorkoutMeasureModel.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import Foundation
import RxSwift
import Eureka

class WorkoutRange {
    let min: Int
    let max: Int

    init(min: Int, max: Int) {
        self.min = min
        self.max = max
    }

    func rangeStrings(withSuffixPluralFormat format: String) -> [String] {
        var result: [String] = []
        for i in min...max {
            result.append(String(format: format, i))
        }
        return result
    }
}

enum WorkoutMeasureType {
    case interval(minutesRange: WorkoutRange, secondsRange: WorkoutRange, variable: Variable<Tuple<String, String>?>)
    case sets(range: WorkoutRange, variable: Variable<String?>)
}

class WorkoutMeasureModel {
    let title: String
    let type: WorkoutMeasureType

    init(title: String, type: WorkoutMeasureType) {
        self.title = title
        self.type = type
    }
}
