//
//  WorkoutDetailsSection.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import Foundation
import RxSwift
import Eureka

class WorkoutDetailsSection {
    let title: String
    let rows: [WorkoutDetailsSectionRow]

    init(title: String, rows: [WorkoutDetailsSectionRow]) {
        self.title = title
        self.rows = rows
    }
}

class WorkoutDetailsSectionRow {
    let type: WorkoutDetailsSectionType

    init(type: WorkoutDetailsSectionType) {
        self.type = type
    }
}

enum WorkoutDetailsSectionType {
    case workoutName(name: String, placeholder: String, variable: Variable<String?>)
    case measureModel(model: WorkoutMeasureModel)
}
