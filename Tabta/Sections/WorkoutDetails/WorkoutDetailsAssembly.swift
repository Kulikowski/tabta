//
//  WorkoutDetailsAssembly.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

class WorkoutDetailsAssembly: Assembly {
    func assemble(container: Container) {
        container.storyboardInitCompleted(WorkoutDetailsViewController.self) { r, c in
            c.viewModel = r.resolve(WorkoutDetailsViewModel.self)!
        }

        container.register(WorkoutDetailsViewModel.self) { r in
            return WorkoutDetailsViewModel(saveWorkoutsUseCase: r.resolve(SaveWorkoutsUseCase.self)!)
        }

        container.register(SaveWorkoutsUseCase.self) { r in
            return SaveWorkoutsUseCase(workoutsRepository: r.resolve(WorkoutsRepository.self)!)
        }

        container.register(WorkoutsRepository.self) { r in
            return WorkoutsRepository(workoutsService: r.resolve(WorkoutsService.self)!)
        }

        container.register(WorkoutsService.self) { _ in
            return WorkoutsService()
        }
    }
}
