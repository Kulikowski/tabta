//
//  WorkoutsRepository.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import Foundation

class WorkoutsRepository {

    let workoutsService: WorkoutsService

    init(workoutsService: WorkoutsService) {
        self.workoutsService = workoutsService
    }

    func getWorkouts(completion: @escaping ((Result<[Workout], Error>) -> Void)) {
        workoutsService.getWorkouts() { [weak self] result in
            guard let `self` = self else {
                //TODO: return a repository based error here
                return
            }
            switch result {
            case .success(let results):
                completion(.success(self.mapResults(from: results)))
            case .error(let error):
                completion(.error(error))
            }
        }
    }

    func save(workout: Workout, completion: @escaping ((Result<Bool, Error>) -> Void)) {
        let parameters = WorkoutParameters(id: workout.id,
                                           name: workout.name,
                                           initialCountdown: workout.initialCountdown,
                                           warmupInterval: workout.warmupInterval,
                                           exerciseInterval: workout.exerciseInterval,
                                           restInterval: workout.restInterval,
                                           numberOfSets: workout.numberOfSets,
                                           recoveryInterval: workout.recoveryInterval,
                                           cooldownInterval: workout.cooldownInterval)
        workoutsService.saveWorkout(withParameters: parameters, completion: completion)
    }

    private func mapResults(from results: [WorkoutManagedObject]) -> [Workout] {
        return results.compactMap({ return Workout(id: $0.objectID.uriRepresentation().absoluteString,
                                                   name: $0.workout_name!,
                                                   exerciseInterval: Int($0.exercise_interval),
                                                   initialCountdown: Int($0.initial_countdown),
                                                   numberOfSets: Int($0.number_of_sets),
                                                   recoveryInterval: Int($0.recovery_interval),
                                                   restInterval: Int($0.rest_interval),
                                                   warmupInterval: Int($0.warmup_interval),
                                                   cooldownInterval: Int($0.cooldown_interval))
        })
    }
}
