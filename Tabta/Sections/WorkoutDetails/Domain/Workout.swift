//
//  Workout.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import Foundation

class Workout: NSObject {
    let id: String?
    let name: String
    let exerciseInterval: Int
    let initialCountdown: Int
    let numberOfSets: Int
    let recoveryInterval: Int
    let restInterval: Int
    let warmupInterval: Int
    let cooldownInterval: Int

    init(id: String? = nil,
         name: String,
         exerciseInterval: Int,
         initialCountdown: Int,
         numberOfSets: Int,
         recoveryInterval: Int,
         restInterval: Int,
         warmupInterval: Int,
         cooldownInterval: Int) {
        self.id = id
        self.name = name
        self.exerciseInterval = exerciseInterval
        self.initialCountdown = initialCountdown
        self.numberOfSets = numberOfSets
        self.recoveryInterval = recoveryInterval
        self.restInterval = restInterval
        self.warmupInterval = warmupInterval
        self.cooldownInterval = cooldownInterval

        super.init()
    }

    func detailsString() -> String {
        let localizedFormat = NSLocalizedString("workout_list__cell__description_format", comment: "")
        return String(format: localizedFormat, exerciseInterval.asTimeString(), initialCountdown.asTimeString(), numberOfSets.asTimeString(), recoveryInterval.asTimeString(), restInterval.asTimeString(), warmupInterval.asTimeString(), cooldownInterval.asTimeString())
    }
}
