//
//  WorkoutsListViewModel.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import UIKit
import RxSwift

class WorkoutsListViewModel: NSObject {

    let getWorkoutsUseCase: GetWorkoutsUseCase

    var isLoading = Variable(false)

    var workouts: Variable<[Workout]> = Variable([])

    var screenTitleString: String {
        return NSLocalizedString("workouts_list__screen_title_string", comment: "")
    }

    var emptySetTitleString: NSAttributedString {
        return NSAttributedString(string: NSLocalizedString("workouts_list__empty_set__title_string", comment: ""))
    }

    var emptySetDetailString: NSAttributedString {
        return NSAttributedString(string: NSLocalizedString("workouts_list__empty_set__detail_string", comment: ""))
    }

    var emptySetIconImage: UIImage? {
        return UIImage(named: "TimerClockIconSmall")
    }

    init(getWorkoutsUseCase: GetWorkoutsUseCase) {
        self.getWorkoutsUseCase = getWorkoutsUseCase
    }

    func reload() {
        self.isLoading.value = true
        getWorkoutsUseCase.getWorkouts { [weak self] result in
            guard let `self` = self else { return }
            switch result {
            case .success(let results):
                self.workouts.value = results
                self.isLoading.value = false
            case .error(_):
                self.isLoading.value = false
                break
            }
        }

    }
}
