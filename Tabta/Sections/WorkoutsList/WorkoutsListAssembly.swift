//
//  WorkoutsListAssembly.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import Swinject
import SwinjectStoryboard

class WorkoutsListAssembly: Assembly {
    func assemble(container: Container) {
        container.storyboardInitCompleted(WorkoutsListTableViewController.self) { r, c in
            c.viewModel = r.resolve(WorkoutsListViewModel.self)!
        }

        container.register(WorkoutsListViewModel.self) { r in
            return WorkoutsListViewModel(getWorkoutsUseCase: r.resolve(GetWorkoutsUseCase.self)!)
        }

        container.register(GetWorkoutsUseCase.self) { r in
            return GetWorkoutsUseCase(workoutsRepository: r.resolve(WorkoutsRepository.self)!)
        }

        container.register(WorkoutsRepository.self) { r in
            return WorkoutsRepository(workoutsService: r.resolve(WorkoutsService.self)!)
        }

        container.register(WorkoutsService.self) { _ in
            return WorkoutsService()
        }
    }
}
