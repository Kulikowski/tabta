//
//  WorkoutsListTableViewController.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift
import RxSwift

class WorkoutsListTableViewController: UITableViewController {

    let disposeBag = DisposeBag()

    var viewModel: WorkoutsListViewModel!

    var selectedWorkout: Workout? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViewModelBindings()

        setupUI()

        viewModel.reload()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.reload()
    }

    // MARK: ViewModel bindings

    private func setupViewModelBindings() {
        viewModel.isLoading.asObservable().subscribe(onNext: { isLoading in
            if !isLoading {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }).disposed(by: disposeBag)
    }

    // MARK: UI setup

    private func setupUI() {
        setupTableView()
        setupNavigationBar()
    }

    private func setupTableView() {
        tableView.separatorStyle = .none
        tableView.emptyDataSetView { view in
            view.titleLabelString(self.viewModel.emptySetTitleString)
                .detailLabelString(self.viewModel.emptySetDetailString)
                .image(self.viewModel.emptySetIconImage)
                .dataSetBackgroundColor(self.tableView.backgroundColor ?? .white)
                .verticalOffset(-80)
                .verticalSpace(20)
                .shouldDisplay(true)
                .shouldFadeIn(true)
                .isTouchAllowed(true)
                .isScrollAllowed(true)
        }

        tableView.estimatedRowHeight = 220
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.register(WorkoutTableViewCell.loadNib(),
                           forCellReuseIdentifier: WorkoutTableViewCell.reuseIdentifier)
    }

    private func setupNavigationBar() {
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.title = self.viewModel.screenTitleString
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(goToAddingWorkout))
    }

    // MARK: UI actions:

    @objc func goToAddingWorkout() {
        self.performSegue(withIdentifier: AppSegues.WorkoutsList.goToAddWorkout, sender: self)
    }

    func goToEditingWorkout(with indexPath: IndexPath) {
        selectedWorkout = self.viewModel.workouts.value[indexPath.row]
        self.performSegue(withIdentifier: AppSegues.WorkoutsList.goToEditWorkout, sender: self)
    }

    // MARK: - Prepare for segue

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }

        switch identifier {
        case AppSegues.WorkoutsList.goToEditWorkout:
            guard let workoutDetailsVC = segue.destination as? WorkoutDetailsViewController else { return }
            guard let selectedWorkout = selectedWorkout else { return }
            workoutDetailsVC.viewModel.update(with: selectedWorkout)
        default:
            break
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return viewModel.workouts.value.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WorkoutTableViewCell.reuseIdentifier,
                                                 for: indexPath)

        let workout = viewModel.workouts.value[indexPath.row]
        if let cell = cell as? WorkoutTableViewCell {
            cell.workoutNameLabel.text = workout.name
            cell.detailsLabel.text = workout.detailsString()
        }
        return cell
    }


    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        goToEditingWorkout(with: indexPath)
    }
}
