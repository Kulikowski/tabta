//
//  GetWorkoutsUseCase.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import Foundation

class GetWorkoutsUseCase: NSObject {
    let workoutsRepository: WorkoutsRepository

    init(workoutsRepository: WorkoutsRepository) {
        self.workoutsRepository = workoutsRepository
    }

    func getWorkouts(completion: @escaping ((Result<[Workout], Error>) -> Void)) {
        self.workoutsRepository.getWorkouts(completion: completion)
    }
}
