//
//  WorkoutTableViewCell.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import UIKit

class WorkoutTableViewCell: UITableViewCell {


    static let reuseIdentifier = "workoutTableViewCell"

    static func loadNib() -> UINib {
        return UINib(nibName: "WorkoutTableViewCell", bundle: nil)
    }

    @IBOutlet var workoutNameLabel: UILabel!
    @IBOutlet var detailsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
