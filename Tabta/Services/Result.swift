//
//  Result.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import Foundation

enum Result<Success, Error> {
    case success(Success)
    case error(Error)

    var isSuccess: Bool {
        if case .success = self {
            return true
        }
        return false
    }
}
