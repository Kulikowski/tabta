//
//  Int+TimeStringMappable.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import Foundation
import Eureka

extension Int {
    func asTimeStringsTuple() -> Tuple<String, String> {
        let minutes = self/60
        let seconds = self % 60

        let minutesPluralFormat = NSLocalizedString("workout_details__measures__minutes_format", comment: "")
        let secondsPluralFormat = NSLocalizedString("workout_details__measures__seconds_format", comment: "")

        let minutesString = String(format: minutesPluralFormat, minutes)
        let secondsString = String(format: secondsPluralFormat, seconds)

        return Tuple<String, String>(a: minutesString, b: secondsString)
    }
}
