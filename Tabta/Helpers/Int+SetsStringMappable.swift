//
//  Int+SetsStringMappable.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import Foundation

extension Int {
    func asSetsString() -> String {
        let setsPluralFormat = NSLocalizedString("workout_details__measures__sets_count_format", comment: "")
        let setsString = String(format: setsPluralFormat, self)
        return "\(setsString)"
    }
}
