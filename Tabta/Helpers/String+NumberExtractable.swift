//
//  String+ToSeconds.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import Foundation

extension String {
    func extractFirstNumber() -> Int? {
        let stringArray = self.components(separatedBy: CharacterSet.decimalDigits.inverted)
        guard let firstNumber = stringArray.first else { return nil }
        return Int(firstNumber)
    }
}
