//
//  Tuple+ToInteger.swift
//  Tabta
//
//  Created by Paweł Kulikowski on 12.09.2018.
//  Copyright © 2018 codemadeapps. All rights reserved.
//

import Foundation
import Eureka

class TupleMapper {
    static func mapToInt(from tuple: Tuple<String, String>?) -> Int? {
        guard let tuple = tuple else { return nil }
        let minutes = tuple.a.extractFirstNumber() ?? 0
        let seconds = tuple.b.extractFirstNumber() ?? 0
        return minutes * 60 + seconds
    }
}
